import React, { Component } from 'react';
import Flat from './flat.jsx';

import flats from '../data/flats.js';


class FlatList extends Component {

  render(){

    return(
      <div className="flat-list">
        {flats.map((flat,index)=> <Flat flat={flat} key={index} index={index} selected={flat.name === this.props.selectedFlat.name} selectFlat={this.props.selectFlat} />)}
      </div>
    )
  }
}

export default FlatList;
